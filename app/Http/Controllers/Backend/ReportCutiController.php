<?php

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use App\Models\ReportCuti;
use App\Models\Departement;
use Illuminate\Http\Request;
//use App\Exports\CutiExport;
use App\Exports\CutiExports;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use DB;

class ReportCutiController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}
	
    public function indexReportCuti() {
		$action = '1';
		$startdate = '';
		$enddate = '';
		$kd_divisi = '';
		$departements = Departement::orderBy('id', 'ASC')->get();
        return view('admin.report.indexReportCuti', compact('startdate', 'enddate', 'kd_divisi', 'action', 'departements'));
    }
	
    public function getTable(Request $request){
		$kd_divisi = $request->input('kd_divisi');
		$startdate = Carbon::parse($request->input('start-date'))->format('Y-m-d');
		$enddate = Carbon::parse($request->input('end-date'))->format('Y-m-d');
		
		$getbulanawal = substr($startdate, 5, 2);
		$gettahunawal = substr($startdate, 0, 4);
		if($getbulanawal == '01'){
			$namabulanawal = 'Januari';
		}else if($getbulanawal == '02'){
			$namabulanawal = 'Februari';	
		}else if($getbulanawal == '03'){
			$namabulanawal = 'Maret';		
		}else if($getbulanawal == '04'){
			$namabulanawal = 'April';			
		}else if($getbulanawal == '05'){
			$namabulanawal = 'Mei';
		}else if($getbulanawal == '06'){
			$namabulanawal = 'Juni';
		}else if($getbulanawal == '07'){
			$namabulanawal = 'Juli';
		}else if($getbulanawal == '08'){
			$namabulanawal = 'Agustus';
		}else if($getbulanawal == '09'){
			$namabulanawal = 'September';
		}else if($getbulanawal == '10'){
			$namabulanawal = 'Oktober';
		}else if($getbulanawal == '11'){
			$namabulanawal = 'November';
		}else{
			$namabulanawal = 'Desember';
		}
		
		
		$getbulanakhir = substr($enddate, 5, 2);
		$gettahunakhir = substr($enddate, 0, 4);
		if($getbulanakhir == '01'){
			$namabulanakhir = 'Januari';
		}else if($getbulanakhir == '02'){
			$namabulanakhir = 'Februari';	
		}else if($getbulanakhir == '03'){
			$namabulanakhir = 'Maret';		
		}else if($getbulanakhir == '04'){
			$namabulanakhir = 'April';			
		}else if($getbulanakhir == '05'){
			$namabulanakhir = 'Mei';
		}else if($getbulanakhir == '06'){
			$namabulanakhir = 'Juni';
		}else if($getbulanakhir == '07'){
			$namabulanakhir = 'Juli';
		}else if($getbulanakhir == '08'){
			$namabulanakhir = 'Agustus';
		}else if($getbulanakhir == '09'){
			$namabulanakhir = 'September';
		}else if($getbulanakhir == '10'){
			$namabulanakhir = 'Oktober';
		}else if($getbulanakhir == '11'){
			$namabulanakhir = 'November';
		}else{
			$namabulanakhir = 'Desember';
		}
		
		$action = '2';
	/*	$data_cuti = DB::select('SELECT MAX(A.TGL_PENGAJUAN_CUTI), A.NIK, A.NAMA_KARYAWAN, A.TGL_PENGAJUAN_CUTI, A.TGL_CUTI_AWAL, 
								A.TGL_CUTI_AKHIR, B.SISA_CUTI_TAHUNAN, B.SISA_CUTI_KHUSUS, B.SISA_CUTI_BESAR , C.DEPARTMENT, C.UNIT
								FROM TBL_PENGAJUAN_CUTI A
								LEFT JOIN DATA_HAK_CUTI B 
								ON A.NIK = B.NIK
								LEFT JOIN DEPARTEMENTS C 
								ON A.KD_DIVISI = C.ID 								
								WHERE kd_divisi = "'.$kd_divisi.'" AND 
								tgl_cuti_awal >= "'.$startdate.'" AND 
								tgl_cuti_akhir <= "'.$enddate.'"
								GROUP BY A.TGL_PENGAJUAN_CUTI, A.NIK, A.NAMA_KARYAWAN, A.TGL_PENGAJUAN_CUTI, A.TGL_CUTI_AWAL, 
								A.TGL_CUTI_AKHIR, B.SISA_CUTI_TAHUNAN, B.SISA_CUTI_KHUSUS, B.SISA_CUTI_BESAR , C.DEPARTMENT, C.UNIT
								ORDER BY A.NIK');	*/
		$data_cuti = DB::select('SELECT A.*, B.*, C.* FROM TBL_PENGAJUAN_CUTI A
								LEFT JOIN DATA_HAK_CUTI B 
								ON A.NIK = B.NIK
								LEFT JOIN DEPARTEMENTS C 
								ON A.KD_DIVISI = C.ID 								
								WHERE kd_divisi = "'.$kd_divisi.'" AND 
								tgl_cuti_awal >= "'.$startdate.'" AND 
								tgl_cuti_akhir <= "'.$enddate.'"
								ORDER BY A.NIK');
		$departements = Departement::orderBy('id', 'ASC')->get();
										
        return view('admin.report.indexReportCuti', compact('data_cuti', 'startdate', 'enddate', 'kd_divisi', 'action', 'namabulanawal', 'gettahunawal',
															'namabulanakhir', 'gettahunakhir', 'departements'
													));
			
    }
	
	
	public function getExport(Request $request){
		$tgl_download =  Carbon::parse($request->input('tgl1'))->format('Ymd').'sd'. Carbon::parse($request->input('tgl2'))->format('Ymd');
		$kd_divisi = $request->input('get_kd_divisi');
		$startdate = Carbon::parse($request->input('tgl1'))->format('Y-m-d');
		$enddate = Carbon::parse($request->input('tgl2'))->format('Y-m-d');
	//	return Excel::download(new CutiExport($kd_divisi, $startdate, $enddate), 'datacuti_'.$tgl_download.'.xlsx');
		return (new CutiExports($kd_divisi, $startdate, $enddate))->download('datacuti_'.$tgl_download.'.xlsx');
	}

}
