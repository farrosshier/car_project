@extends('admin.partial.app')
@section('content')
<div class="inner">
                    <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Approve Pengajuan Cuti</h1>
                </div>
            </div>
            <div class="row">
                    
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
							Approve Pengajuan Cuti
                        </div>
						
						<form action="{{url('/backend/store/cuti/approve_cuti')}}" method="post">
							{{csrf_field()}}
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											Form Cuti
										</div>
										<div class="col-lg-8">
											<div class="row">
												<div class="panel panel-default">
													<div class="panel-body">
														<h4>A. IDENTITAS PEMOHON</h4>
														<div class="col-lg-6" hidden>
															<label>Id</label>
															<div class="form-group">
																<input class="form-control" name="id" id="id" value="{{$id}}" readonly />
															</div>
														</div>
														<div class="col-lg-6">
															<label>Nama Karyawan</label>
															<div class="form-group">
																<input class="form-control" name="nama_karyawan" id="nama_karyawan" value="{{$nama_karyawan}}" readonly />
															</div>
															<div class="form-group">
																<label>Jabatan</label>
																<input class="form-control" name="jabatan" id="jabatan" value="{{$jabatan}}" readonly>
															</div>
															<div class="form-group">
																<label>Div/Dept/Bag</label>
																<input class="form-control" name="kd_divisi" id="kd_divisi" value="{{$kd_divisi}}" readonly>
															</div>
														</div>
														<div class="col-lg-6">
															<div class="form-group">
																<label>NIK</label>
																<input class="form-control" name="nik" id="nik" value="{{$nik}}" readonly >
															</div>
															<div class="form-group">
																<label>TMK</label>
																<input class="form-control" name="tmk" id="tmk" value="{{$tgl_masuk_karyawan}}" readonly>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="panel panel-default">
													<div class="panel-body">
														<h4>B. PERIODE PERMOHONAN CUTI/IJIN</h4>
														<div class="col-lg-3">
															<div class="form-group">
																<label>Start Date</label>
																<input class="form-control" name="tgl_cuti_awal" id="tgl_cuti_awal" value="{{$tgl_cuti_awal}}" readonly >
															</div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label>End Date :</label>
																<input class="form-control" name="tgl_cuti_akhir" id="tgl_cuti_akhir" value="{{$tgl_cuti_akhir}}" readonly >
															</div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label>Jumlah Hari Kerja :</label>
																<input id="jumlah_hari" name="jumlah_hari" class="form-control" value="{{$jumlah_hari}}" readonly>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="panel panel-default">
													<div class="panel-body">
														<h4>C. ALASAN CUTI/IJIN</h4>
														<div class="row">
															<div class="col-lg-6">
																<div class="form-group">
																	<input class="form-control" name="jenis_cuti" id="jenis_cuti" value="{{$jenis_cuti}}" readonly >
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="panel panel-default">
													<div class="panel-body">
														<h4>E. PENJELASAN CUTI/IJIN/KETIDAKHADIRAN</h4>
														<div class="form-group">
															<textarea name="penjelasan_cuti" id="penjelasan_cuti" class="form-control" rows="3" value="{{$penjelasan_cuti}}" readonly>{{$penjelasan_cuti}}</textarea>
														</div>
													</div>
												</div> 
												
												<div class="panel panel-default">
													<div class="panel-body">
														<h4>G. Alamat yang bisa dihubungi selama cuti/ijin tidak masuk kerja :</h4>
														<h6><i>(untuk kepentingan keadaan darurat)</i></h6>
														<div class="form-group">
															<textarea name="alamat" id="alamat" class="form-control" rows="3" value="{{$penjelasan_cuti}}" readonly>{{$penjelasan_cuti}}</textarea>
														</div>
													</div>
												</div>
												
												<div class="panel panel-default">
													<div class="panel-body">
														<div class="form-group">
															<div class="row">
																<div class="col-md-6">
																	<h4>H. Usulan Petugas Pengganti	:</h4>
																</div>
															</div>
															<div class="row">	
																<div class="col-md-6">
																	<div class="form-group">
																		<input id="petugas_pemimpin" name="petugas_pemimpin" class="form-control" value="{{$petugas_pengganti}}" readonly>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<div class="col-lg-4">
											<div class="row">
												<div class="panel panel-default">
													<div class="panel-body">
														<h4>D. PERHITUNGAN CUTI TAHUNAN</h4>
														<div class="col-lg-12">
															<div class="form-group">
																<label>Hak cuti hari sebelumnya</label>
																<input class="form-control" name="hak_cuti_tahunan" id="hak_cuti_tahunan" value="{{$sisa_cuti_tahunan}}" disabled> hari
															</div>
															<div class="form-group">
																<label>Telah diambil</label>
																<input class="form-control" name="cuti_tahunan_sudah_diambil" id="cuti_tahunan_sudah_diambil" value="{{$sisa_cuti_tahunan_sudah_diambil}}" disabled> hari
															</div>
															<div class="form-group">
																<label>Sisa sebelum diambil</label>
																<input class="form-control" name="cuti_tahunan_sebelum_diambil" id="cuti_tahunan_sebelum_diambil" value="{{$sisa_cuti_tahunan_diambil}}" disabled> hari
															</div>
															<div class="form-group">
																<label>Akan diambil</label>
																<input class="form-control" name="cuti_tahunan_akan_diambil" id="cuti_tahunan_akan_diambil" value="<?php if($jenis_cuti == 'C1' || $jenis_cuti == 'C2' || $jenis_cuti == 'C3' || $jenis_cuti == 'C4'){ echo $jumlah_hari;} else{echo ''; } ?>" disabled> hari
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="panel panel-default">
													<div class="panel-body">
														<h4>D. PERHITUNGAN CUTI KHUSUS</h4>
														<div class="col-lg-12">
															<div class="form-group">
																<label>Hak cuti hari sebelumnya</label>
																<input class="form-control" name="hak_cuti_khusus" id="hak_cuti_khusus" value="{{$sisa_cuti_khusus}}" disabled> hari
															</div>
															<div class="form-group">
																<label>Telah diambil</label>
																<input class="form-control" name="cuti_khusus_sudah_diambil" id="cuti_khusus_sudah_diambil" value="{{$sisa_cuti_khusus_sudah_diambil}}" disabled> hari
															</div>
															<div class="form-group">
																<label>Sisa sebelum diambil</label>
																<input class="form-control" name="cuti_khusus_sebelum_diambil" id="cuti_khusus_sebelum_diambil" value="{{$sisa_cuti_khusus_diambil}}" disabled> hari
															</div>
															<div class="form-group">
																<label>Akan diambil</label>
																<input class="form-control" name="cuti_khusus_akan_diambil" id="cuti_khusus_akan_diambil"  value="<?php if($jenis_cuti == 'C5'){ echo $jumlah_hari;} else{echo ''; } ?>" disabled> hari
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="panel panel-default">
													<div class="panel-body">
														<h4>Catatan<h4>
														<div class="col-lg-12">
															<ol>
																<li>Formulir harus dilengkapi dengan baik.</li>
																<li>Cuti & ijin akan dipotong setelah approval.</li>
																<li>Segala bentuk ijin/ketidakhadiran (kecuali sakit akan dipotong cuti tahunan).</li>
																<li>Ketidakhadiran tanpa alasan yang sah akan dipotong dari cuti tahunan.</li>
															</ol>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
                            </div>
							<br><br><br>
							<?php
								if($tombol == 'aktif'){
							?>
							<div class="row">
								<div class="col-lg-6">
									<div class="col-lg-12">
										<label>Action:</label>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<div class="radio">
												<label>
													<input type="radio" name="action" id="action" value="Y" >Setujui
												</label>
											</div>
										</div>	
									</div>	
									<div class="col-lg-4">
										<div class="form-group">
											<div class="radio">
												<label>
													<input type="radio" name="action" id="action" value="N" >Tolak
												</label>
											</div>
										</div>	
									</div>		
								</div>
							</div>
							<br>
							
							<div class="col-md-12">
								<button class="btn btn-success"><i class="icon-ok"></i>Simpan</button>
							</div>
							<?php
								}else{
									
								}
							?>
                        </div>
						
						</form>
                    </div>
                </div>
            </div>
        </div>
@endsection