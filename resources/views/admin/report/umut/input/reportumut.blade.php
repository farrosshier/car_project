  <div class="col-md-12" style="width: 1032px; overflow:auto;">
  <div>
  <table class="table table-bordered" width="1200">
      <thead>
          <tr>
              <th rowspan="2" bgcolor="#6cddbf" style="vertical-align: middle;"><div align="center">No</div></th>
              <th valign="center" bgcolor="#6cddbf" style="vertical-align: middle;"><div align="center" style="width: 100px;">NIK</div></th>
              <th valign="center" bgcolor="#6cddbf" style="vertical-align: middle;"><div align="center" style="width: 150px;">Nama</div></th>
              <th valign="center" bgcolor="#6cddbf" style="vertical-align: middle;"><div align="center">Total UT</div></th>
              <th valign="center" bgcolor="#6cddbf" style="vertical-align: middle;"><div align="center" style="width: 100px;">UT</div></th>
              <th valign="center" bgcolor="#6cddbf" style="vertical-align: middle;"><div align="center" style="width: 100px;">Sub Total UT</div></th>
              <th valign="center" bgcolor="#6cddbf" style="vertical-align: middle;"><div align="center">Total UM</div></th>
              <th valign="center" bgcolor="#6cddbf" style="vertical-align: middle;"><div align="center" style="width: 100px;">UM</div></th>
              <th valign="center" bgcolor="#6cddbf" style="vertical-align: middle;"><div align="center" style="width: 100px;">Sub Total UM</div></th>
              <th valign="center" bgcolor="#6cddbf" style="vertical-align: middle;"><div align="center" style="width: 100px;">Jumlah</div></th>
          </tr>
     </thead>
     <tbody>
        @foreach($departments as $key => $value)
            <tr>
                <td colspan="10" bgcolor="#c5ed4e"><b>{{$value->unit}}</b></td>
            </tr>
            <tr>
              @php
              $employees = \App\Models\Employee::where('dept_id',$value->id)->get();
              @endphp
              <?php
                $z=0;
                $totalut = 0;
                $totalum = 0;
                $jumlahkeseluruhan = 0;
              ?>
              @foreach($employees as $i => $employee)
              @if((!in_array($employee->ut, ['Lumpsum','N/A'])) || (!in_array($employee->um, ['Kupon','N/A'])))
              <?php
                $z++;
                $lemburs = \DB::table('absensi')->where('nik',$employee->nik)->whereBetween('date',[$dateFrom,$dateTo])
                                    ->where(function($query){
                                            $query->where('mrly','>',180)
                                                  ->orWhere('plate','>',180);
                                        })
                                    ->get();
                $datelembur=[];
                $p=0;
                foreach ($lemburs as $lembur) {
                  if($employee->level == 'Non Staff'){
                    $datelembur[$p]=$lembur->date;
                    $datelembur[$p++];
                  }else{
                    if((!in_array($lembur->date, $getSatDays)) || (in_array($lembur->date, $getSunDays)) || (in_array($lembur->date, $tglMerah))){ //bukan hari sabtu, atau tgl merah, atau liburr tetap dihitung um
                      $datelembur[$p]=$lembur->date;
                      $datelembur[$p++];
                    }
                  }
                }
                $datalembur = count($datelembur);

                $absensis = \App\Models\Absensi::where('nik',$employee->nik)->whereBetween('date',[$dateFrom,$dateTo])
                                    ->whereNotNull('wjm')->whereNotNull('wjm')->get(); //BELUM ADA KONDISI KETERANGAN
                $dateabsen=[];
                $h=0;
                foreach ($absensis as $key => $absensi) {
                  if($employee->level == 'Non Staff'){
                    $dateabsen[$h]=$absensi->date;
                    $dateabsen[$h++];
                  }else{
                    if((!in_array($absensi->date, $getSatDays)) || (in_array($absensi->date, $getSunDays)) || (in_array($absensi->date, $tglMerah))){ //bukan hari sabtu, atau tgl merah, atau liburr tetap dihitung um
                      $dateabsen[$h]=$absensi->date;
                      $dateabsen[$h++];
                    }
                  }
                }
                $absensium = count($dateabsen)+$datalembur;
                /*UT*/
                if((!in_array($employee->ut, ['Lumpsum','N/A']))&&(in_array($employee->level, ['Staff','Non Staff']))&&($employee->jabatan == 'Sales Consultant')){ //Get uang transport sales consultant
                  $ut = \App\Models\MstBiaya::where('type','UT')->where('code','A')->first();
                  $ut = $ut->amount;
                }elseif((!in_array($employee->ut, ['Lumpsum','N/A']))&&(in_array($employee->level, ['Staff','Non Staff']))&&($employee->jabatan == 'Sales Counter')){
                  $ut = \App\Models\MstBiaya::where('type','UT')->where('code','B')->first();
                  $ut = $ut->amount;
                }elseif((!in_array($employee->ut, ['Lumpsum','N/A']))&&(in_array($employee->level, ['Staff','Non Staff']))&&(!in_array($employee->jabatan,['Sales Counter','Sales Consultant']))){
                  $ut = \App\Models\MstBiaya::where('type','UT')->where('code','C')->first();
                  $ut = $ut->amount;
                }else{
                  $ut = 0;
                }
                /*END UT*/
                /*UM*/
                if((!in_array($employee->um, ['Kupon','N/A']))&&(in_array($employee->jabatan, ['General Manager','Sales & Marketing Manager','Sales Consultant','Sales Supervisor','Sales Counter']))){
                  $um = \App\Models\MstBiaya::where('type','UM')->where('code','A')->first();
                  $um = $um->amount;
                }elseif((!in_array($employee->um, ['Kupon','N/A']))&&(in_array($employee->level, ['Staff','Non Staff']))&&(!in_array($employee->jabatan, ['General Manager','Sales & Marketing Manager','Sales Consultant','Sales Supervisor','Sales Counter']))){
                  $um = \App\Models\MstBiaya::where('type','UM')->where('code','B')->first();
                  $um = $um->amount;
                }
                else{
                  $um=0;
                }
                /*END UM*/
                $subtotalut=count($absensis)*$ut;
                $subtotalum=$absensium*$um;
                $totalut = $totalut + $subtotalut;
                $totalum = $totalum + $subtotalum;
                $jumlah = $subtotalut+$subtotalum;
                $jumlahkeseluruhan = $jumlahkeseluruhan + $jumlah;
                
              ?>
              <tr>
                <td>{{$z}}</td>
                <td>{{$employee->nik}}</td>
                <td>{{$employee->nama}}</td>
                <td><div align="center"><b>{{count($absensis)}}</b></div></td>
                <td><div align="center"><b>{{number_format($ut, 0)}}</b></div></td>
                <td><div align="center"><b>{{number_format($subtotalut, 0)}}</b></div></td>
                <td><div align="center"><b>{{$absensium}}</b></div></td>
                <td><div align="center"><b>{{number_format($um, 0)}}</b></div></td>
                <td><div align="center"><b>{{number_format($subtotalum,0)}}</b></div></td>
                <td><div align="center"><b>{{number_format($jumlah,0)}}</b></div></td>
              </tr>
              @endif
              @endforeach
            </tr>
            <tr>
              <td colspan="3"><div align="center"><b>Total</b></div></td>
              <td><div align="center"><b>-</b></div></td>
              <td><div align="center"><b>-</b></div></td>
              <td><div align="center"><b>{{number_format($totalut,0)}}</b></div></td>
              <td><div align="center"><b>-</b></div></td>
              <td><div align="center"><b>-</b></div></td>
              <td><div align="center"><b>{{number_format($totalum,0)}}</b></div></td>
              <td><div align="center"><b>{{number_format($jumlahkeseluruhan,0)}}</b></div></td>
            </tr>
        @endforeach
     </tbody>
</table>
</div>
</div>