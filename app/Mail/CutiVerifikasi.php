<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Cuti;

class CutiVerifikasi extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($subject, $nama_karyawan, $nik, $divisi, $jabatan,
                                $tgl_pengajuan_cuti, $tgl_cuti_awal, $tgl_cuti_akhir,
                                $jumlah_hari, $penjelasan_cuti, $atasan, $id)
    {
        $this->subject = $subject;
        $this->nama_karyawan = $nama_karyawan;
        $this->nik = $nik;
        $this->divisi = $divisi;
        $this->jabatan = $jabatan;
        $this->tgl_pengajuan_cuti = $tgl_pengajuan_cuti;
        $this->tgl_cuti_awal = $tgl_cuti_awal;
        $this->tgl_cuti_akhir = $tgl_cuti_akhir;
        $this->jumlah_hari = $jumlah_hari;
        $this->penjelasan_cuti = $penjelasan_cuti;
        $this->atasan = $atasan;
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->subject;
        $nama_karyawan = $this->nama_karyawan;
        $nik = $this->nik;
        $divisi = $this->divisi;
        $jabatan = $this->jabatan;
        $tgl_pengajuan_cuti = $this->tgl_pengajuan_cuti;
        $tgl_cuti_awal = $this->tgl_cuti_awal;
        $tgl_cuti_akhir = $this->tgl_cuti_akhir;
        $jumlah_hari = $this->jumlah_hari;
        $penjelasan_cuti = $this->penjelasan_cuti;
        $atasan = $this->atasan;
        $id = $this->id;

        $link = "http://127.0.0.1:8000/";
        
        return $this->view('emails.cutiverifikasi', compact('subject', 'nama_karyawan', 'nik', 'divisi', 
                                                        'jabatan', 'tgl_pengajuan_cuti','tgl_cuti_awal',
                                                        'tgl_cuti_akhir', 'jumlah_hari', 'penjelasan_cuti', 'link',
                                                        'atasan', 'id'));


    }


}
