<?php

/*namespace App\Exports;

//use App\Lembur;
use App\Models\Lembur;
use Maatwebsite\Excel\Concerns\FromCollection;

class LemburExport implements FromCollection
{
	 public function __construct($nik, $bulan, $tahun)
    {
        $this->nik = $nik;
        $this->bulan = $bulan;
        $this->tahun = $tahun;
    }
	
    public function collection()
    {
		return Lembur::where('nik', $this->nik)
						->where(\DB::raw('substr(tgl_pengajuan_lembur, 6, 2)'), $this->bulan)
						->where(\DB::raw('substr(tgl_pengajuan_lembur, 1, 4)'), $this->tahun)->get();
    }
}	*/



namespace App\Exports;

use App\Models\Lembur;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class LemburExport implements FromView
{
	use Exportable;
	
	public function __construct($nik, $bulan, $tahun)
    {
        $this->nik = $nik;
        $this->bulan = $bulan;
        $this->tahun = $tahun;
    }
	
	public function view(): View
    {
        return view('admin.lembur.reportlemburtemplate', [
		
									
			'lembur' => DB::select("SELECT 
									(CASE
									WHEN C.LEVEL = 'Non Staff' THEN (SELECT TIME_SCHEDULE_AWAL FROM shift_schedules A LEFT JOIN SCHEDULES B ON A.DEPT = B.DEPT_ID WHERE A.DEPT = A.kd_divisi )
									ELSE
									(SELECT CONCAT(WJM, ':00') FROM ABSENSI WHERE NIK = '".$this->nik."' AND DATE = SUBSTRING(A.TGL_LEMBUR_AWAL, 1, 11))
									END) AS mulai_shift,
									(CASE
									WHEN C.LEVEL = 'Non Staff' THEN (SELECT TIME_SCHEDULE_AKHIR FROM shift_schedules A LEFT JOIN SCHEDULES B ON A.DEPT = B.DEPT_ID WHERE A.DEPT = A.kd_divisi )
									ELSE
									(SELECT CONCAT(WJK, ':00') FROM ABSENSI WHERE NIK = '".$this->nik."' AND DATE = SUBSTRING(A.TGL_LEMBUR_AWAL, 1, 11))
									END) AS selesai_shift,
									A.TGL_LEMBUR_AWAL, A.TGL_LEMBUR_AKHIR,
									(SELECT WJM FROM ABSENSI WHERE NIK = '".$this->nik."' AND DATE = SUBSTRING(A.TGL_LEMBUR_AWAL, 1, 11)) AS wjm,
									(SELECT WJK FROM ABSENSI WHERE NIK = '".$this->nik."' AND DATE = SUBSTRING(A.TGL_LEMBUR_AWAL, 1, 11)) AS wjk,

									A.*, C.*
									FROM TBL_PENGAJUAN_LEMBUR A
									LEFT JOIN EMPLOYEES C
									ON C.NIK = A.NIK
									WHERE A.NIK = '".$this->nik."'
									AND SUBSTRING(A.TGL_PENGAJUAN_LEMBUR, 6, 2) = '".$this->bulan."'
									AND SUBSTRING(A.TGL_PENGAJUAN_LEMBUR, 1, 4) = '".$this->tahun."'
									")	
        ]);
    }
	
}
