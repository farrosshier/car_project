@extends('admin.partial.app')
@section('content')
<div class="inner">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Lembur</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lembur
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											Form Lembur
										</div>
										<div class="col-lg-12">
											<div class="row">
												<div class="panel panel-default">
													<div class="panel-body">
														<!--form action="{{url('/backend/store/lembur/pengajuan_lembur')}}" method="post"-->
														{{csrf_field()}}
														<div class="col-lg-6">
															<div class="row">
																<div class="col-lg-12">
																	<div class="col-lg-4">
																		<label>Nama</label>
																	</div>
																	<div class="col-lg-8">
																		<?php echo $getuser ?>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-lg-12">
																	<div class="col-lg-4">
																		<label>NIK</label>
																	</div>
																	<div class="col-lg-8">
																		<div class="form-group">
																			<input name="nik" id="nik" value="{{$parameter['nik']}}" class="form-control" required readonly>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-lg-12">
																	<div class="col-lg-4">
																		<label>Jabatan</label>
																	</div>
																	<div class="col-lg-8">
																		<div class="form-group">
																			<input name="jabatan" id="jabatan" value="{{$parameter['level']}}" class="form-control" required readonly>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-lg-12">
																	<div class="col-lg-4">
																		<label>Div / Dept / Bagian</label>
																	</div>
																	<div class="col-lg-8">
																		<div class="form-group">
																			<select id="kd_divisi" name="kd_divisi" class="form-control" required>
																				<option value=""selected disabled>PILIH DEPARTEMENT</option>
																				@foreach($departements as $departement)
																				<option value="{{$departement->id}}">{{$departement->department}}-{{$departement->unit}}</option>
																				@endforeach
																			</select>
																		</div>
																	</div>
																</div>
															</div>
															
															<div class="row">
																<div class="col-lg-12">
																	<div class="col-lg-4">
																		<label>Tanggal Lembur</label>
																	</div>
																	<div class="col-lg-8">
																		<div class="form-group">
																			<div class="col-md-12">
																				<div class="input-group date" data-provide="datepicker">
																					<input type="text" id="tgl_lembur_awal" name="tgl_lembur_awal" class="form-control datecalendar" onchange="hari();">
																					<div class="input-group-addon">
																						<span class="glyphicon glyphicon-th"></span>
																					</div>
																				</div>
																				<input type="text" id="nama_hari" name="nama_hari" class="form-control" style="display:none;">
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<br>
															<div class="col-lg-12">
																<div class="row">
																	<div class="col-lg-4">
																		<label>Jam Lembur Awal & Akhir</label>
																	</div>
																	<div class="col-lg-8">
																		<div class="form-group">
																			<div class="col-lg-6">
																				<div class="input-group ">
																					<input class="form-control timeselector" id="jam_lembur_awal" name="jam_lembur_awal" type="time" />
																					<span class="input-group-addon add-on"><i class="icon-time"></i></span>
																				</div>
																			</div>
																			<div class="col-lg-6">
																				<div class="input-group">
																					<input class="form-control timeselector" id="jam_lembur_akhir" name="jam_lembur_akhir" type="time" />
																					<span class="input-group-addon add-on"><i class="icon-time"></i></span>
																				</div>
																			</div>

																		</div>
																	</div>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-lg-12">
																	<div class="col-lg-4">
																		<label>Keterangan</label>
																	</div>
																	<div class="col-lg-8">
																		<div class="form-group">
																			<textarea name="keterangan_lembur" id="keterangan_lembur" class="form-control" rows="3" required></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<button class="btn btn-success" onclick="saveLembur();"><i class="icon-ok"></i>Ajukan</button>
																<!--button class="btn btn-success"><i class="icon-ok"></i>Ajukan</button-->
															</div>
														</div>
														<!--/form-->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-lg-12">
				@include('admin.cuti.modalKaryawan')
			</div>	
		</div>
	</div>
	
	<script type="text/javascript">
		$('.timeselector').timespinner({
  			format: 'HH:mm'
		});
	</script>
	<script>
		$('.datepicker').datepicker({
			format: 'mm/dd/yyyy',
			startDate: '-3d'
		});
	</script>
	
	
@endsection