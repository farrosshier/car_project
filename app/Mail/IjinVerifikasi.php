<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Ijin;

class IjinVerifikasi extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($subject, $nama_karyawan, $nik, $divisi, $jabatan,
                                $tgl_pengajuan_ijin, $tgl_jam_ijin_awal, $tgl_jam_ijin_akhir,
                                $keterangan_ijin, $atasan, $id)
    {
        $this->subject = $subject;
        $this->nama_karyawan = $nama_karyawan;
        $this->nik = $nik;
        $this->divisi = $divisi;
        $this->jabatan = $jabatan;
        $this->tgl_pengajuan_ijin = $tgl_pengajuan_ijin;
        $this->tgl_jam_ijin_awal = $tgl_jam_ijin_awal;
        $this->tgl_jam_ijin_akhir = $tgl_jam_ijin_akhir;
        $this->keterangan_ijin = $keterangan_ijin;
        $this->atasan = $atasan;
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->subject;
        $nama_karyawan = $this->nama_karyawan;
        $nik = $this->nik;
        $divisi = $this->divisi;
        $jabatan = $this->jabatan;
        $tgl_pengajuan_ijin = $this->tgl_pengajuan_ijin;
        $tgl_jam_ijin_awal = $this->tgl_jam_ijin_awal;
        $tgl_jam_ijin_akhir = $this->tgl_jam_ijin_akhir;
        $keterangan_ijin = $this->keterangan_ijin;
        $atasan = $this->atasan;
        $id = $this->id;

        $link = "http://127.0.0.1:8000/";
        
        return $this->view('emails.ijinverifikasi', compact('subject', 'nama_karyawan', 'nik', 'divisi', 
                                                        'jabatan', 'tgl_pengajuan_ijin','tgl_jam_ijin_awal',
                                                        'tgl_jam_ijin_akhir', 'keterangan_ijin', 'link',
                                                        'atasan', 'id'));

    }


}
