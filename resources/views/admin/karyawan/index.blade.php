@extends('admin.partial.app')
@section('content')
<div class="inner">
<div class="row">
    <div class="col-lg-12">
        <h2> Master Karyawan </h2>
    </div>
</div>
<hr />
<div class="row">
    <div class="col-lg-12">
        <!-- <div class="panel panel-default">
            <div class="panel-heading">
                Master Karyawan
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                	<div class="col-md-12" style="padding-bottom: 60px;">
                		<div class="col-md-8">
                            <div class="col-md-5">
                                <select name="dept_id" class="form-control" required>
                                    @foreach($departements as $departement)
                                    <option value="{{$departement->id}}">{{$departement->department}}-{{$departement->unit}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-success" onclick="getDays()">Reload Data</button>
                            </div>
                        </div>
                	</div>
                    <div class="col-md-12 table-responsive" id="karyawan">
                    	
                    </div>
                </div>
            </div>
        </div> -->
        <div class="box dark">
            <header>
                <h5>Master Karyawan</h5>
                <div class="toolbar">
                    <ul class="nav">
                        <li><a href="{{url('/')}}/backend/karyawan/add"><i class="icon-plus-sign"> Tambah Karyawan</i></a></li>
                    </ul>
                </div>
            </header>
            <div id="div-1" class="accordion-body collapse in body">
                <div class="table-responsive">
                    <div class="col-md-12" style="padding-bottom: 35px;">
                        <div class="col-md-8">
                            <div class="col-md-5">
                                <select name="dept_id" class="form-control" required>
                                    @foreach($departements as $departement)
                                    <option value="{{$departement->id}}">{{$departement->department}}-{{$departement->unit}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-success" onclick="getGridKaryawan()">Reload Data</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 table-responsive" id="karyawan" style="padding: 0;">

        </div>
    </div>
</div>
</div>
@endsection