<?php

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use App\Models\MstBiaya;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}
    public function indexbiaya() {
    	$mstbiaya = MstBiaya::all();
    	return view('admin.setting.biaya.index',compact('mstbiaya'));
    }
    public function updatebiaya($id,Request $request) {
    	$amount = str_replace(".","",$request->input('amount'));
    	$mstbiaya = MstBiaya::find($id);
    	$mstbiaya->amount = $amount;
    	$mstbiaya->update();
    	flash()->success('Biaya Berhasil Di Update');
    	return redirect()->back();
    }

}
