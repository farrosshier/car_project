<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Lembur;

class LemburVerifikasi extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($subject, $nama_karyawan, $nik, $divisi, $jabatan,
                                $tgl_pengajuan_lembur, $tgl_lembur_awal, $tgl_lembur_akhir,
                                $lama_lembur, $keterangan_lembur, $atasan, $id)
    {
        $this->subject = $subject;
        $this->nama_karyawan = $nama_karyawan;
        $this->nik = $nik;
        $this->divisi = $divisi;
        $this->jabatan = $jabatan;
        $this->tgl_pengajuan_lembur = $tgl_pengajuan_lembur;
        $this->tgl_lembur_awal = $tgl_lembur_awal;
        $this->tgl_lembur_akhir = $tgl_lembur_akhir;
        $this->lama_lembur = $lama_lembur;
        $this->keterangan_lembur = $keterangan_lembur;
        $this->atasan = $atasan;
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->subject;
        $nama_karyawan = $this->nama_karyawan;
        $nik = $this->nik;
        $divisi = $this->divisi;
        $jabatan = $this->jabatan;
        $tgl_pengajuan_lembur = $this->tgl_pengajuan_lembur;
        $tgl_lembur_awal = $this->tgl_lembur_awal;
        $tgl_lembur_akhir = $this->tgl_lembur_akhir;
        $lama_lembur = $this->lama_lembur;
        $keterangan_lembur = $this->keterangan_lembur;
        $atasan = $this->atasan;
        $id = $this->id;

        $link = "http://127.0.0.1:8000/";
        
        return $this->view('emails.lemburverifikasi', compact('subject', 'nama_karyawan', 'nik', 'divisi', 
                                                        'jabatan', 'tgl_pengajuan_lembur','tgl_lembur_awal',
                                                        'tgl_lembur_akhir', 'lama_lembur', 'keterangan_lembur', 'link',
                                                        'atasan', 'id'));

    /*    return $this->from('farros.jackson@gmail.com', 'Farros Shier')
                    ->view('emails.verifikasi2')
                    ->with([
                            'name' => 'Joe Doe',
                            'link' => 'http://www.bryceandy.com'
                      ])
                      ->attach(public_path('/images').'/demo.jpg', [
                              'as' => 'demo.jpg',
                              'mime' => 'image/jpeg',
                      ]);   */

    }


}
