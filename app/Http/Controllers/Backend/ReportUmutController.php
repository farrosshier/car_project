<?php

namespace App\Http\Controllers\Backend;

use DateTime;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use App\Models\Departement;
use App\Models\TanggalMerah;
use App\Models\Absensi;
use Illuminate\Http\Request;
use App\Imports\AbsensiImport;
use App\Imports\ImportAbsen;
use App\Models\Employee;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ReportUmutController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}
    public function index() {
    	return view('admin.report.umut.index');
    }

    public function getDataReportUmut(Request $request){
        $departments = Departement::all();
        $periode = $request->input('periode');
        if($periode ==1){
            $dateFrom = $request->input('year')."-".$request->input('month')."-01";
            $dateTo = date("Y-m-t", strtotime($dateFrom));
            $endperiod  = (new DateTime($dateTo))->modify('+1 day')->format('Y-m-d');
        }else{
            $dateFrom = DateTime::createFromFormat('d/m/Y', $request->input('dateFrom'))->format('Y-m-d');
            $datetime = DateTime::createFromFormat('d/m/Y', $request->input('dateTo'))->format('Y-m-d');
            $dateTo   = (new DateTime($datetime))->format('Y-m-d');
            $endperiod  = (new DateTime($datetime))->modify('+1 day')->format('Y-m-d');
        }
        $start = new DateTime($dateFrom);
        $end = new DateTime($endperiod);
        $interval = DateInterval::createFromDateString('1 days');
        $period   = new DatePeriod($start, $interval, $end);
        $days=[];
        $h=0;
        foreach ($period as $dt) {
            $list = $dt->format("Y m d-D");
            $day = substr($list, -3);
            if($day == 'Sat'){
             $days[$h] = $dt->format("Y-m-d");
             $days[$h++];
            }
        }
        $getSatDays = $days;
        $sunday=[];
        $j=0;
        foreach ($period as $dt) {
            $list = $dt->format("Y m d-D");
            $day = substr($list, -3);
            if($day == 'Sun'){
             $sunday[$j] = $dt->format("Y-m-d");
             $sunday[$j++];
            }
        }
        $getSunDays = $sunday;
        $tglMerah = TanggalMerah::whereBetween('date',[$dateFrom,$dateTo])->get()->pluck('date');
        // return $tglMerah;
        if(count($tglMerah)>0){
            $tglMerah = $tglMerah;
        }else{
            $tglMerah = ['0','0'];
        }
        return view('admin.report.umut.input.reportumut',compact(
                        'departments','dateTo','dateFrom','getSatDays','getSunDays','tglMerah'
                    ));
    }

}
