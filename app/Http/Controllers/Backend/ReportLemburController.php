<?php

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use App\Models\ReportCuti;
use App\Models\Employee;
use App\Models\Lembur;
use App\Models\Departement;
use Illuminate\Http\Request;
use App\Exports\LemburExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use DB;

class ReportLemburController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}
    
    public function indexReportLembur() {
		$nik = '';
		$nama_karyawan = '';
		$bulan = date('m');
		$tahun = date('Y');
		$action = '1';
		$data_karyawan = DB::select('SELECT A.*, B.*
										FROM EMPLOYEES A 
										LEFT JOIN DEPARTEMENTS B ON A.DEPT_ID = B.ID');
		$data_lembur = Lembur::orderBy('tgl_pengajuan_lembur', 'DESC')->get();	
		$departements = Departement::orderBy('id', 'ASC')->get();	
		
        return view('admin.lembur.indexReportLembur', compact('data_karyawan', 'data_lembur', 'bulan', 'tahun', 
																'action', 'nama_karyawan', 'nik', 'departements'
													));
    }
	
	public function getTable(Request $request){
		$nik = $request->input('nik');
		$nama_karyawan = $request->input('nama_karyawan');
		$kd_divisi = $request->input('kd_divisi');
		$bulan = $request->input('bulan');
		$tahun = $request->input('tahun');
		$action = '2';
		
		$data_karyawan = Employee::orderBy('dept_id', 'ASC')->get();
	/*	$data_lembur = Lembur::where('nik', $nik)
								->where(\DB::raw('substr(tgl_pengajuan_lembur, 6, 2)'), '=' , $bulan)
								->where(\DB::raw('substr(tgl_pengajuan_lembur, 1, 4)'), '=' , $tahun)->get();	*/
	/*	$data_lembur = DB::select('SELECT A.*, B.* 
									FROM TBL_PENGAJUAN_LEMBUR A 
									LEFT JOIN ABSENSI B 
									ON A.NIK = B.NIK 
									WHERE A.NIK = "'.$nik.'" 
									AND SUBSTRING(A.TGL_PENGAJUAN_LEMBUR, 6, 2) = "'.$bulan.'"
									AND SUBSTRING(A.TGL_PENGAJUAN_LEMBUR, 1, 4) = "'.$tahun.'"');	*/
		$data_lembur = DB::select("SELECT 
									(CASE
									WHEN C.LEVEL = 'Non Staff' THEN (SELECT TIME_SCHEDULE_AWAL FROM shift_schedules A LEFT JOIN SCHEDULES B ON A.DEPT = B.DEPT_ID WHERE A.DEPT = A.kd_divisi )
									ELSE
									(SELECT CONCAT(WJM, ':00') FROM ABSENSI WHERE NIK = '".$nik."' AND DATE = SUBSTRING(A.TGL_LEMBUR_AWAL, 1, 11))
									END) AS mulai_shift,
									(CASE
									WHEN C.LEVEL = 'Non Staff' THEN (SELECT TIME_SCHEDULE_AKHIR FROM shift_schedules A LEFT JOIN SCHEDULES B ON A.DEPT = B.DEPT_ID WHERE A.DEPT = A.kd_divisi )
									ELSE
									(SELECT CONCAT(WJK, ':00') FROM ABSENSI WHERE NIK = '".$nik."' AND DATE = SUBSTRING(A.TGL_LEMBUR_AWAL, 1, 11))
									END) AS selesai_shift,
									A.TGL_LEMBUR_AWAL, A.TGL_LEMBUR_AKHIR,
									(SELECT CONCAT(WJM, ':00') FROM ABSENSI WHERE NIK = '".$nik."' AND DATE = SUBSTRING(A.TGL_LEMBUR_AWAL, 1, 11)) AS WJM,
									(SELECT CONCAT(WJK, ':00') FROM ABSENSI WHERE NIK = '".$nik."' AND DATE = SUBSTRING(A.TGL_LEMBUR_AWAL, 1, 11)) AS WJK,

									A.*, C.*
									FROM TBL_PENGAJUAN_LEMBUR A
									LEFT JOIN EMPLOYEES C
									ON C.NIK = A.NIK
									WHERE A.NIK = '".$nik."'
									AND SUBSTRING(A.TGL_PENGAJUAN_LEMBUR, 6, 2) = '".$bulan."'
									AND SUBSTRING(A.TGL_PENGAJUAN_LEMBUR, 1, 4) = '".$tahun."'");
		
		$departements = Departement::orderBy('id', 'ASC')->get();
										
		return view('admin.lembur.indexReportLembur', compact('data_karyawan', 'data_lembur', 'bulan','tahun','action','nama_karyawan', 'nik', 
																'kd_divisi', 'departements'));
	}
	
	public function getExport(Request $request){
		$nik = $request->input('get_nik');
		$bulan = $request->input('get_bulan');
		$tahun = $request->input('get_tahun');
		$download = $nik.'_'.$bulan.'_'.$tahun;
		return Excel::download(new LemburExport($nik, $bulan, $tahun), 'datalembur_'.$download.'.xlsx');
	}
	
	

}
