
 <?php
	$action = $action;
	if($action == '2'){
 
 ?>

<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
	<div class="col-md-12 table-responsive" id="ReportLemburTable">
		<div class="col-md-12">
			<table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" aria-describedby="dataTables-example_info">
				<thead>
					<tr role="row">
						<th rowspan="2" colspan="1">No</th>
						<th rowspan="2" colspan="1">Nama Pegawai / NIK</th>
						<th rowspan="2" colspan="1">Dept / Unit</th>
						<th rowspan="2" colspan="1">TGL</th>
						<th rowspan="2">Jenis K/L</th>
						<th align="center" rowspan="1" colspan="3">Waktu Lembur</th>
						<th rowspan="2" colspan="1">Keterangan</th>
						<th rowspan="1" colspan="3">Diketahui</th>
					</tr>
					<tr role="row">
						<th rowspan="1" colspan="1">Mulai (Form)</th>
						<th rowspan="1" colspan="1">Selesai (Form)</th>
						<th rowspan="1" colspan="1">Jumlah</th>
						<th rowspan="1" colspan="1">1</th>
						<th rowspan="1" colspan="1">2</th>
						<th rowspan="1" colspan="1">3</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$no = 0;
					?>
					@foreach($data_lembur as $data)
					
					<?php
						$no = $no+1;
						$tgl_lembur = substr($data->tgl_lembur_awal, 0, 11);
						$jam_awal = substr($data->tgl_lembur_awal, 10, 9);
						$jam_akhir = substr($data->tgl_lembur_akhir, 10, 9);
						
					?>
					<tr class="gradeA odd">
						<th><?php echo $no; ?></th>
						<th>{{$data->nama_karyawan}} / {{$data->nik}}</th>
						<th>{{$data->kd_divisi}}</th>
						<td><?php echo $tgl_lembur; ?></td>
						<td>{{$data->jenis_lembur}}</td>
						<td><?php echo $jam_awal; ?></td>
						<td><?php echo $jam_akhir; ?></td>
						<td>{{$data->lama_lembur}}</td>
						<td>{{$data->keterangan_lembur}}</td>
						<td>{{$data->app_name1}}</td>
						<td>{{$data->app_name2}}</td>
						<td class="center ">{{$data->app_name3}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-3">
		</div>
		<div class="col-md-3">
		</div>
		<div class="col-md-3">
		</div>
		<div class="col-md-3">
			<div class="form-group">
			<form action="{{url('/backend/rekap/rekap_lembur/export')}}" method="POST">
				{{csrf_field()}}
				<input class="form-control" name="get_nama_karyawan" id="get_nama_karyawan" style="display:none;" />
				<input id="get_nik" name="get_nik" value="{{$nik}}" class="form-control" style="display:none;" >
				<input id="get_bulan" name="get_bulan" value="{{$bulan}}" class="form-control" style="display:none;" >
				<input id="get_tahun" name="get_tahun" value="{{$tahun}}" class="form-control" style="display:none;" >
				<button href="" type="submit" class="btn btn-success">Print</button>
			</form>
			</div>
		</div>
	</div>
</div>

<?php
	}else{
	}
?>