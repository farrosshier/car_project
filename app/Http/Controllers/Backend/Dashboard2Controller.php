<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Mail;
use DateTime;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use App\Models\Cuti;
use App\Models\Employee;
use App\Models\Departement;
use App\Models\Ijin;
use App\Models\Lembur;
use App\Models\Absensi;
use App\Models\TanggalMerah;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Dashboard2Controller extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}
	
	public function checkAuth(){
    	return redirect('/'); // change to here;
    }
	

    public function index(Request $request) {
    //	return redirect('/backend/jadwal/nonshift'); //jika sudah kelar hapus ini
		$user = Auth::user();
		$nama = $user->name;
		
    	if($user->hasRole('Staff') || $user->hasRole('Non Staff')){
			$filter = "a.nama = '".$nama."' 
						and b.app3 != '' ";
		}else if($user->hasRole('Supervisor')){
			$filter = "(case 
							when b.nama_karyawan != '".$nama."' 
								then a.direct_supervisor = '".$nama."' 
									and a.direct_supervisor != a.next_higher_supervisor 
									and b.id != '' 
									and b.app1 != 'Y' 
							else 
								b.app3 != '' 
						end)";
		}else if($user->hasRole('Manager')){
				$filter = "(case 
							when b.nama_karyawan != '".$nama."' 
								then a.next_higher_supervisor = '".$nama."' 
									and
									(case
										when a.direct_supervisor = a.next_higher_supervisor
											then b.app2 = '' and b.app1 = ''
										else b.app2 = '' and b.app1 = 'Y'
									end) 
							else 
								b.app1 = '' 
						end)";
		}else if($user->hasRole('Management')){
			$filter = " b.nama_karyawan = '".$nama."' and b.app1 = '' ";
		}else if($user->hasRole('Super Admin')){
			$filter = "b.app3 = '' and b.app2 = 'Y'";
		}else{
			$filter = "b.app1 = 'HHHHH'";
		}

		$data_cuti = DB::select("select a.*, b.* 
								from employees a 
								left join tbl_pengajuan_cuti b 
								on b.nama_karyawan = a.nama 
								where ".$filter." ");
		$data_lembur = DB::select("select a.*, b.* 
								from employees a 
								left join tbl_pengajuan_lembur b 
								on b.nama_karyawan = a.nama 
								where ".$filter." ");
		$data_ijin = DB::select("select a.*, b.* 
								from employees a 
								left join tbl_pengajuan_ijin b 
								on b.nama_karyawan = a.nama 
								where ".$filter." ");
		
		if($user->hasRole('Super Admin') || $user->hasRole('Management') || $user->hasRole('Manager') || $user->hasRole('Supervisor')){
			$cuti_url = "http://127.0.0.1:8000/backend/cuti/approve_cuti";
			$ijin_url = "http://127.0.0.1:8000/backend/ijin/approve_ijin";
			$lembur_url = "http://127.0.0.1:8000/backend/lembur/approve_lembur";
			$post = "post";
		}else{
			$cuti_url = "";
			$ijin_url = "";
			$lembur_url = "";
			$post = "";
		}

		return view('admin.dashboard2', compact('data_cuti', 'data_ijin', 'data_lembur', 'cuti_url', 'ijin_url', 'lembur_url', 'post'));
	
    }
	
	public function getChart() {
		$karyawanaktif = DB::table('Employees')->where('status', '1')->count();
		$karyawannonaktif = DB::table('Employees')->where('status', '0')->count();
		
		$result = array(
			'aktif'=>$karyawanaktif,
			'nonaktif'=>$karyawannonaktif
		);
		
		return response()->json($result);
		
	}

	public function sendMail(){
		$contactemail = 'abdrozak20@gmail.com';
		$user = 'aaaaaa';
		// dd($user);
		Mail::send('emails.testmail', compact('user'), function ($message) use ($contactemail) {
		     $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_ADDRESS'));
		     $message->subject('Reschedule Interview');
		     $message->to($contactemail);
		 });
		flash()->success('success');
		return redirect()->back();
	}
}
