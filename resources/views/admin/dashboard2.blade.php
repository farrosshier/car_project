@extends('admin.partial.app')
@section('content')
<div class="inner">
<div class="row">
    <div class="col-lg-12">
        <h1> Admin Dashboard </h1>
    </div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-danger">
			<div class="panel-heading">
				NOTIFIKASI PENGAJUAN
			</div>
			<?php
				class waktu_pengajuan{
					protected $tgl_pengajuan;
					
					public function __construct($tgl_pengajuan){
						$this->tgl_pengajuan = $tgl_pengajuan;
						
						$diff = abs(strtotime(date('Y-m-d H:i:s')) - strtotime($this->tgl_pengajuan));
			
						$years   = floor($diff / (365*60*60*24)); 
						$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
						$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
						
						$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60)); 
						$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 
						
						if($days > 0){
							$waktu = $days.' hari yang lalu';
						}else{
							if($hours > 0){
								$waktu = $hours.' jam yang lalu';
							}else{
								$waktu = $minuts.' menit yang lalu';
							}
						}
						echo $waktu;
					}
				}
			?>
			<div class="panel-body" style="height:500px; overflow:scroll;">
				@foreach($data_cuti as $datacuti)
				<div class="alert alert-success" name="pengajuan_cuti" value="{{$datacuti->id}}">
					<form action="{{$cuti_url}}" method="{{$post}}">
					{{csrf_field()}}
						<button type="submit" name="approvecuti" id="approvecuti" value="{{$datacuti->id}}" class="btn btn-success">
							Pengajuan Cuti
						</button> : {{$datacuti->nama_karyawan}} 
						<a style="float:right;">
							<?php 
								$waktu_cuti = new waktu_pengajuan($datacuti->tgl_pengajuan_cuti); 
							?>
						</a>
					</form>
				</div>
				@endforeach
				
				@foreach($data_ijin as $dataijin)
				<div class="alert alert-warning" name="pengajuan_ijin" value="{{$dataijin->id}}">
					<form action="{{$ijin_url}}" method="{{$post}}">
					{{csrf_field()}}
						<button type="submit" name="approveijin" id="approveijin" value="{{$dataijin->id}}" class="btn btn-warning">
							Pengajuan Ijin
						</button> : {{$dataijin->nama_karyawan}} 
						<a style="float:right;">
							<?php
								$waktu_ijin = new waktu_pengajuan($dataijin->tgl_pengajuan_ijin); 
							?>
						</a>
					</form>
				</div>
				@endforeach
				
				@foreach($data_lembur as $datalembur)
				<div class="alert alert-info" name="pengajuan_lembur" value="{{$datalembur->id}}">
					<form action="{{$lembur_url}}" method="{{$post}}">
					{{csrf_field()}}
						<button type="submit" name="approvelembur" id="approvelembur" value="{{$datalembur->id}}" class="btn btn-info">
							Pengajuan Lembur
						</button> : {{$datalembur->nama_karyawan}} 
						<a style="float:right;">
							<?php
								$waktu_lembur = new waktu_pengajuan($datalembur->tgl_pengajuan_lembur); 
							?>
						</a>
					</form>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	<div class="col-md-6">
			
		<div class="panel panel-default">
			
			<div class="panel-heading">
				Grafik Karyawan Aktif & Non Aktif
			</div>
			<div class="panel-body">
				
				<div class="demo-container">
					<div id="placeholder" class="demo-placeholder"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<hr />
</div>
@endsection

