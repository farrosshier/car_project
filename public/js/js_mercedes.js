function cutiKhusus($jenis_cuti, $jenis_cuti_detail){
	var jenis_cuti = $jenis_cuti;
	var keterangan_cuti_div = document.getElementById("keterangan_cuti_div");
	if(jenis_cuti == 'C5'){	
		keterangan_cuti_div.style.display = 'block';
		var keterangan_cuti = document.getElementById("keterangan_cuti").value;
	}else{
		keterangan_cuti_div.style.display = 'none';
		var keterangan_cuti = '';
	}
}

function inputJumlahCuti($jenis_cuti){
	var jumlah_hari = $('#jumlah_hari');
	var nik = $('#nik').val();
	var keterangan_cuti = $('#keterangan_cuti').val();
	var tgl_cuti_awal = $("#tgl_cuti_awal");
	var tgl_cuti_akhir = $("#tgl_cuti_akhir");
	if($jenis_cuti == 'C5'){
		jumlah_hari.prop('readonly', true);
		tgl_cuti_awal.prop('readonly', true);
		tgl_cuti_akhir.prop('readonly', true);
	}else{
		if($jenis_cuti == 'C4'){	
			data = {		
				_token : token,
				nik : nik
			};
			console.log(nik);
			$.ajax({
				url: base_url+'/backend/cuti/cuti_besar',
				type: 'POST',
				data: data,
			})
			.done(function(msg) {
				if(msg < 6){
					$.alert({	
						title: 'Alert!',
						type: 'red',
						content: 'Anda belum berhak mendapatkan hak cuti besar!',
					});
					jumlah_hari.prop('readonly', true);
					tgl_cuti_awal.prop('readonly', true);
					tgl_cuti_akhir.prop('readonly', true);
					jumlah_hari.val('');
				}else{
					jumlah_hari.prop('readonly', false);
					tgl_cuti_awal.prop('readonly', false);
					tgl_cuti_akhir.prop('readonly', false);
					jumlah_hari.val('');
				}
			})
			.fail(function() {
				console.log('gagal')
			});	
		}else{
			jumlah_hari.prop('readonly', false);
			tgl_cuti_awal.prop('readonly', false);
			tgl_cuti_akhir.prop('readonly', false);
			jumlah_hari.val('');
		}
	}
	$('#kode_cuti').val($jenis_cuti);
}


function lamaCuti($jenis_cuti_detail){
	var jenis_cuti_detail = $jenis_cuti_detail;
	var tgl_cuti_awal = $(".tgl_cuti_awal").val();
	var tgl_cuti_akhir = $(".tgl_cuti_akhir").val();
	var jenis_cuti_dtl = jenis_cuti_detail.substr(0, 2);
	var jumlah_hari = document.getElementById("jumlah_hari");


	if(jenis_cuti_detail == '1' || jenis_cuti_detail == '2' || jenis_cuti_detail == '3' || jenis_cuti_detail == '4'){
		data = {		
			_token : token,
			tgl_cuti_awal : tgl_cuti_awal,
			tgl_cuti_akhir : tgl_cuti_akhir
		};
		$.ajax({
			url: base_url+'/backend/cuti/lamacutikaryawan',
			type: 'POST',
			data: data,
		})
		.done(function(msg) {
			if(tgl_cuti_awal != '' && tgl_cuti_akhir != ''){
				$("#jumlah_hari").val(msg);
				$("#cuti_tahunan_akan_diambil").val(msg);
				$('#cuti_khusus_akan_diambil').val('');
			}else{
				$("#jumlah_hari").val('');
				$("#cuti_tahunan_akan_diambil").val('');
				$('#cuti_khusus_akan_diambil').val('');
			}
		})
		.fail(function() {
			console.log('gagal');
		});	
	}else if(jenis_cuti_detail == '5'){
		var jumlahhari = '3';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val(jumlahhari);
		$('#jumlah_hari').val(jumlahhari);
		
	}else if(jenis_cuti_detail == '6'){
		var jumlahhari = '2';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val(jumlahhari);
		$('#jumlah_hari').val(jumlahhari);
	}else if(jenis_cuti_detail == '7'){
		var jumlahhari = '2';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val(jumlahhari);
		$('#jumlah_hari').val(jumlahhari);
	}else if(jenis_cuti_detail == '8'){
		var jumlahhari = '2';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val(jumlahhari);
		$('#jumlah_hari').val(jumlahhari);
	}else if(jenis_cuti_detail == '9'){
		var jumlahhari = '2';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val(jumlahhari);
		$('#jumlah_hari').val(jumlahhari);
	}else if(jenis_cuti_detail == '10'){
		var jumlahhari = '2';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val(jumlahhari);
		$('#jumlah_hari').val(jumlahhari);
	}else if(jenis_cuti_detail == '11'){
		var jumlahhari = '1';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val(jumlahhari);
		$('#jumlah_hari').val(jumlahhari);
	}else if(jenis_cuti_detail == '12'){
		var jumlahhari = '40';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val(jumlahhari);
		$('#jumlah_hari').val(jumlahhari);
	}else if(jenis_cuti_detail == '13'){
		var jumlahhari = '90';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val('');
		$('#jumlah_hari').val(jumlahhari);
	}else if(jenis_cuti_detail == '14'){
		var jumlahhari = '45';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val('');
		$('#jumlah_hari').val(jumlahhari);
	}else{
		var jumlahhari = '';
		$('#cuti_tahunan_akan_diambil').val('');
		$('#cuti_khusus_akan_diambil').val('');
		$('#jumlah_hari').val('');
	}
	
	
}



function saveCuti(){
	var nama_karyawan = $('#nama_karyawan').val();
	var nik = $('#nik').val();
	var kd_divisi = $('#kd_divisi').val();
	var jabatan = $('#jabatan').val();
	var tgl_cuti_awal = $('.tgl_cuti_awal').val();
	var tgl_cuti_akhir = $('.tgl_cuti_akhir').val();
	var jumlah_hari = $('#jumlah_hari').val();
	var tmk = $('#tmk').val();
	var jenis_cuti = $('#jenis_cuti').val();
	var kode_cuti = $('#kode_cuti').val();
	var penjelasan_cuti = $('#penjelasan_cuti').val();
	var alamat = $('#alamat').val();
    var petugas_pengganti = $('#petugas_pengganti').val();
	
	$('.loading').removeClass('hide');
	data = {		
		_token:token,
		nama_karyawan : nama_karyawan,
		nik : nik,
		kd_divisi : kd_divisi,
		jabatan : jabatan,
		tgl_cuti_awal : tgl_cuti_awal,
		tgl_cuti_akhir : tgl_cuti_akhir,
		jumlah_hari : jumlah_hari,
		tmk : tmk,
		jenis_cuti : jenis_cuti,
		kode_cuti : kode_cuti,
		penjelasan_cuti : penjelasan_cuti,
		alamat :alamat,
		petugas_pengganti : petugas_pengganti
	};
	
	$.ajax({
		url: base_url+'/backend/store/cuti/pengajuan_cuti',
		type: 'POST',
		data: data,
	})
	.done(function(data) {
		$('.loading').addClass('hide');
		$.alert({
			title: 'Success!',
			type: 'green',
			content: 'Thank you!',
		});
		location. reload(true);
	})
	.fail(function() {
		$('.loading').addClass('hide');
		console.log("");
		$.alert({
			title: 'Alert!',
			type: 'red',
			content: 'Please try again',
		});
	});
}

function saveLembur(){
	var nama_karyawan = $('#nama_karyawan').val();
	var nik = $('#nik').val();
	var kd_divisi = $('select[name="kd_divisi"]').val();
	var jabatan = $('#jabatan').val();
	var tgl_lembur_awal = $('#tgl_lembur_awal').val();
	var jam_lembur_awal = $('#jam_lembur_awal').val();
	var tgl_lembur_akhir = $('#tgl_lembur_awal').val();
	var jam_lembur_akhir = $('#jam_lembur_akhir').val();
	var keterangan_lembur = $('#keterangan_lembur').val();
	
	$('.loading').removeClass('hide');
	data = {		
		_token:token,
		nama_karyawan : nama_karyawan,
		nik : nik,
		kd_divisi : kd_divisi,
		jabatan : jabatan,
		tgl_lembur_awal : tgl_lembur_awal,
		tgl_lembur_akhir : tgl_lembur_awal,
		jam_lembur_awal : jam_lembur_awal,
		jam_lembur_akhir : jam_lembur_akhir,
		keterangan_lembur : keterangan_lembur
	};
	
	console.log(data);
	$.ajax({
		url: base_url+'/backend/store/lembur/pengajuan_lembur',
		type: 'POST',
		data: data,
	})
	.done(function(data) {
		$('.loading').addClass('hide');
		$.alert({
			title: 'Success!',
			type: 'green',
			content: 'Thank you!',
		});
		location. reload(true);
	})
	.fail(function() {
		$('.loading').addClass('hide');
		console.log("");
		$.alert({
			title: 'Alert!',
			type: 'red',
			content: 'Please try again',
		});
	});
}



function saveIjin(){
	var nama_karyawan = $('#nama_karyawan').val();
	var nik = $('#nik').val();
	var kd_divisi = $('select[name="kd_divisi"]').val();
	var jabatan = $('#jabatan').val();
	var tgl_ijin_awal = $('#tgl_ijin_awal').val();
	var jam_ijin_awal = $('#jam_ijin_awal').val();
	var tgl_ijin_akhir = $('#tgl_ijin_akhir').val();
	var jam_ijin_akhir = $('#jam_ijin_akhir').val();
	var tindak_lanjut = $("input[name='tindak_lanjut']:checked"). val();
	var keterangan_ijin = $('#keterangan_ijin').val();
	console.log(tgl_ijin_akhir);
	$('.loading').removeClass('hide');
	data = {		
		_token:token,
		nama_karyawan : nama_karyawan,
		nik : nik,
		kd_divisi : kd_divisi,
		jabatan : jabatan,
		tgl_ijin_awal : tgl_ijin_awal,
		tgl_ijin_akhir : tgl_ijin_akhir,
		jam_ijin_awal : jam_ijin_awal,
		jam_ijin_akhir : jam_ijin_akhir,
		tindak_lanjut : tindak_lanjut,
		keterangan_ijin : keterangan_ijin
	};
	
	console.log(data);
	$.ajax({
		url: base_url+'/backend/store/ijin/pengajuan_ijin',
		type: 'POST',
		data: data,
	})
	.done(function(data) {
		$('.loading').addClass('hide');
		$.alert({
			title: 'Success!',
			type: 'green',
			content: 'Thank you!',
		});
		location. reload(true);
	})
	.fail(function() {
		$('.loading').addClass('hide');
		$.alert({
			title: 'Alert!',
			type: 'red',
			content: 'Please try again',
		});
	});
}


function filterkaryawan(){
	var dept_id = $('#kd_divisi').val();
	data = {
		'_token': token,
		dept_id: dept_id
	};
	$.ajax({
		url: base_url + '/backend/post/cuti/filterkaryawan',
		type: 'POST',
		data: data,
		success: function (data) {	
			$('#isi_data').html(data);
		}
	});
}


function post() {
	var table = document.getElementsByTagName("table")[0];
	var tbody = table.getElementsByTagName("tbody")[0];
	tbody.onclick = function (e) {
		e = e || window.event;
		var data = [];
		var target = e.srcElement || e.target;
		while (target && target.nodeName !== "TR") {
			target = target.parentNode;
		}
		if (target) {
			var cells = target.getElementsByTagName("td");
			for (var i = 0; i < cells.length; i++) {
				data.push(cells[i].innerHTML);
				dt = data.toString();
			}
		}
	
		dt = data.toString();
		dt_split = dt.split(",");
		var nik = dt_split[1];
		data = {
			'_token': token,
			nik: nik
		};
		$.ajax({
			url: base_url + '/backend/post/cuti/user',
			type: 'POST',
			data: data,
			success: function (data) {
<<<<<<< HEAD
				console.log(data);
=======
>>>>>>> e24ee200d0a1999e8236b81f82be386659e7ea79
				dt = data[0].toString();
				dt_split = dt.split(",");	
				console.log(data[0]);
				$('#nama_karyawan').val(data[0]['nama']);
				$('#nik').val(data[0]['nik']);
				$('#jabatan').val(data[0]['level']);
				$('#kd_divisi').val(data[0]['dept_id']);
				$('#tmk').val(data[0]['tgl_join']);
				$('#hak_cuti_tahunan').val(data[0]['sesudah']);
				$('#cuti_tahunan_sudah_diambil').val(data[0]['cutitahunan']);
				$('#cuti_tahunan_sebelum_diambil').val(data[0]['sesudah']);
				$('#hak_cuti_khusus').val(data[0]['sesudah']);
				$('#cuti_khusus_sudah_diambil').val(data[0]['cutikhusus']);
				$('#cuti_khusus_sebelum_diambil').val(data[0]['sesudah']);
			}
		});
		
		$("#formModal").modal('hide');
	};
}

function hari(){
	var eventDate = $('.datecalendar').val();
	var dateElement = eventDate.split("/");
	var dateFormat = dateElement[2]+'-'+dateElement[0]+'-'+dateElement[1];
	var date = new Date(dateFormat+'T10:00:00Z'); //To avoid timezone issues
	var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var day = weekday[date.getDay()];
	$('#nama_hari').val(day);
	console.log(day);
}


function cari_data(){
	var $rows = $('#isi_data tr');
	$('#search').keyup(function() {
		var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
		
		$rows.show().filter(function() {
			var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
			return !~text.indexOf(val);
		}).hide();
	});
}