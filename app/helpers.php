<?php

use Carbon\Carbon;
use App\Models\TanggalMerah;
use Illuminate\Support\Facades\DB;


function limitWord($str, $limit) {
  $word = \Illuminate\Support\Str::words($str, $limit, '...');
  return $word;
}

function cekTglMerah($date){
    $cek = TanggalMerah::where('date',$date)->get();
    if(count($cek)>0){
        $data = true;
    }
    else{
        $data = false;
    }
    return $data;
}

function getDays($start_date){
    $start_time = strtotime($start_date);

    $end_time = strtotime("+1 month", $start_time);
    $days=[];
    $h=0;
    for($i=$start_time; $i<$end_time; $i+=86400)
    {
       $list = date('Y m d-D', $i);
       $days[$h]["tgl"] = date('Y-m-d',$i);
       $days[$h]["days"] = substr($list, -3);
       $days[$h++];
    }
    $getDays = $days;
    return $getDays;
}

function getSatDays($start_date){
    $start_time = strtotime($start_date);
    $end_time = strtotime("+1 month", $start_time);
    $days=[];
    $h=0;
    for($i=$start_time; $i<$end_time; $i+=86400)
    {
       $list = date('Y m d-D', $i);
       $day = substr($list, -3);
       if($day == 'Sat'){
        $days[$h] = date('Y-m-d',$i);
        $days[$h++];
       }
    }
    $getSatDays = $days;
    return $getSatDays;
}

function transformDate($value)
{
    try {
        return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
    } catch (\ErrorException $e) {
        return Carbon::parse($value)->format('Y-m-d');
    }
}

function transformTime($value)
{
    try {
        return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
    } catch (\ErrorException $e) {
        return Carbon::parse($value)->format('H:i');
    }
}

function getWKHR($tgl,$time_schedule_awal,$time_schedule_akhir){
    $jamMasuk = new DateTime($time_schedule_awal);
    $jamPulang = new DateTime($time_schedule_akhir);
    $range = $jamPulang->diff($jamMasuk);
    $range=date_create($range->format('%H:%I'));
    if(!in_array($tgl, getSatDays($tgl))){
       date_sub($range,date_interval_create_from_date_string("1 hour")); 
    }
    $wkhr = date_format($range,"H:i");
    $wkhr = explode(':', $wkhr);
    $wkhr = ($wkhr[0] * 60.0 + $wkhr[1] * 1.0);
    return $wkhr;
}

function getMonths(){
    $months[] = (object) array('id' => '01', 'M' => 'Jan');
    $months[] = (object) array('id' => '02', 'M' => 'Feb');
    $months[] = (object) array('id' => '03', 'M' => 'Mar');
    $months[] = (object) array('id' => '04', 'M' => 'Apr');
    $months[] = (object) array('id' => '05', 'M' => 'Mei');
    $months[] = (object) array('id' => '06', 'M' => 'Jun');
    $months[] = (object) array('id' => '07', 'M' => 'Jul');
    $months[] = (object) array('id' => '08', 'M' => 'Agu');
    $months[] = (object) array('id' => '09', 'M' => 'Sep');
    $months[] = (object) array('id' => '10', 'M' => 'Okt');
    $months[] = (object) array('id' => '11', 'M' => 'Nov');
    $months[] = (object) array('id' => '12', 'M' => 'Des');
    return $months;
}

function updatecuti($nik) {
    if($nik != ''){
        $tahun = date('Y');
        $datacuti = DB::table('data_hak_cuti')->where('nik',$nik)->where('tahun',$tahun)->first();
        $sisa = $datacuti->sisa_cuti_tahunan - 0.5;
        DB::table('data_hak_cuti')->where('nik',$nik)->where('tahun',$tahun)->update(['sisa_cuti_tahunan'=>$sisa]);
        // $updatedatacuti = DB::select("UPDATE DATA_HAK_CUTI 
        //                             SET SISA_CUTI_TAHUNAN = (SELECT (SISA_CUTI_TAHUNAN - 0.5) As SCT) 
        //                             WHERE NIK ='$nik' 
        //                             AND TAHUN ='$tahun' ");
        $msg = 'BERHASIL UPDATE!';
        return ($msg);
    }else{
        $msg = 'GAGAL UPDATE!';
        return ($msg);
    }
    
}