<?php

namespace App\Http\Controllers\Backend;

use DateTime;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use App\Models\Departement;
use App\Models\TanggalMerah;
use App\Models\Absensi;
use Illuminate\Http\Request;
use App\Imports\AbsensiImport;
use App\Imports\ImportAbsen;
use App\Models\Employee;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ReportAbsensiController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}
    public function indexUpload() {
        $nowYear = date('Y');
        $departements = Departement::all();
        return view('admin.report.absensi.index_upload',compact('nowYear','departements'));
    }

    public function uploadAbsensi(Request $request){
        (new ImportAbsen)->forEvent($request)->import($request->file('absensi'), 'local', \Maatwebsite\Excel\Excel::XLSX);
        flash()->success('Berhasil menyimpan data');
        return redirect()->back();
    }
	
    public function index() {
    	return view('admin.report.absensi.index');
    }

    public function getDepartment(){
        $departmens = Departement::all();
        return view('admin.report.absensi.input.department',compact('departmens'));
    }

    public function getDataReportAbsensi(Request $request)
    {
        $tipe = $request->input('tipe');
        $berdasarkan = $request->input('berdasarkan');
        $dept_id = $request->input('department');
        $periode = $request->input('periode');
        if ($berdasarkan == 1) {
            $names = Employee::where('dept_id',$dept_id)->get();
        }else{
            $names = Departement::all();
        }
        if($tipe == 1){
            if($periode ==1){
                $dateFrom = $request->input('year')."-".$request->input('month')."-01";
                $dateTo = date("Y-m-t", strtotime($dateFrom));
                $endperiod  = (new DateTime($dateTo))->modify('+1 day')->format('Y-m-d');
                $start = new DateTime($dateFrom);
                $end = new DateTime($dateTo);
                $totalhari = $start->diff($end)->days+1;
            }else{
                $dateFrom = DateTime::createFromFormat('d/m/Y', $request->input('dateFrom'))->format('Y-m-d');
                $datetime = DateTime::createFromFormat('d/m/Y', $request->input('dateTo'))->format('Y-m-d');
                $dateTo   = (new DateTime($datetime))->format('Y-m-d');
                $endperiod  = (new DateTime($datetime))->modify('+1 day')->format('Y-m-d');
                $start = new DateTime($dateFrom);
                $end = new DateTime((new DateTime($datetime))->format('Y-m-d'));
                $totalhari = $start->diff($end)->days+1;
            }
            $countTglMerah = TanggalMerah::whereBetween('date',[$dateFrom,$dateTo])->count();
            $start = new DateTime($dateFrom);
            $end = new DateTime($endperiod);
            $interval = DateInterval::createFromDateString('1 days');
            $period   = new DatePeriod($start, $interval, $end);
            $days=[];
            $h=0;
            foreach ($period as $dt) {
                $list = $dt->format("Y m d-D");
                $day = substr($list, -3);
                if($day == 'Sun'){
                 $days[$h] = $dt->format("Y-m-d");
                 $days[$h++];
                }
            }
            $getSunDays = count($days);
            $jumlahharikerja = $totalhari-$getSunDays-$countTglMerah;

            return view('admin.report.absensi.input.reportAbsensi',compact(
                    'names','berdasarkan','dateFrom','dateTo','jumlahharikerja'
            ));
        }else{
            $now = Carbon::now()->format('m');
            $months = getMonths();
            $data=[];
            foreach ($months as $month) {
                if($month->id <= $now){
                    $data[] = $month;
                }
            }
            $months = $data;
            $tahun = $request->input('tahun');
            return view('admin.report.absensi.input.reportAbsensiAkumulatif',compact('names','berdasarkan','months','tahun'));

        }
    }
}
