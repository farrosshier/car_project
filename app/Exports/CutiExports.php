<?php

/*namespace App\Exports;

use App\Models\Cuti;
use Illuminate\Contracts\View\View;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;
use Exportable;
​
class CutiExports implements FromView
{
    
    
/*	 public function __construct($kd_divisi, $startdate, $enddate)
    {
        $this->kd_divisi = $kd_divisi;
        $this->startdate = $startdate;
        $this->enddate = $enddate;
    }	*/
	
/*    public function view(): View
    {
        return view('admin.cuti.reportcutitemplate', [
            'Cuti' => Cuti::all()
        ]);
    }
}	*/

namespace App\Exports;

use App\Models\Cuti;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class CutiExports implements FromView
{
	use Exportable;
	
	public function __construct($kd_divisi, $startdate, $enddate)
    {
        $this->kd_divisi = $kd_divisi;
        $this->startdate = $startdate;
        $this->enddate = $enddate;
    }
	
	public function view(): View
    {
        return view('admin.cuti.reportcutitemplate', [
			'cuti' => DB::select('SELECT A.*, B.*, C.* 
								FROM TBL_PENGAJUAN_CUTI A
								LEFT JOIN DATA_HAK_CUTI B 
								ON A.NIK = B.NIK
								LEFT JOIN DEPARTEMENTS C 
								ON A.KD_DIVISI = C.ID 								
								WHERE kd_divisi = "'.$this->kd_divisi.'" AND 
								tgl_cuti_awal >= "'.$this->startdate.'" AND 
								tgl_cuti_akhir <= "'.$this->enddate.'"
								ORDER BY b.nik')
        ]);
    }
	
}