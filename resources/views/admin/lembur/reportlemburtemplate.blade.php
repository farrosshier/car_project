<table border='1'>
    <thead>
        <tr>
			<td colspan="15" align="center">DATA LEMBUR</td> 
		</tr>
		<tr>
			<td></td> 
		</tr>
	</thead>
</table>


<table border='1'>
    <thead>
        <tr>
            <th>No</th>
            <th>Tgl Lembur</th>
            <th>Jenis Lembur</th>
            <th>NIK</th>
            <th>Nama Pegawai</th>
            <th>Mulai Lembur (Form)</th>
            <th>Selesai Lembur (Form)</th>
            <th>Mulai Lembur (Finger)</th>
            <th>Selesai Lembur (Finger)</th>
			<th>Mulai Lembur (Valid)</th>
            <th>Selesai Lembur (Valid)</th>
            <th>Dept / Unit</th>
            <th>Lama Break (Jam)</th>
            <th>Lama Lembur</th>
            <th>Keterangan</th>
            <th>Approval 1</th>
            <th>Approval 2</th>
            <th>Approval 3</th>
        </tr>
    </thead>
    <tbody>
    <?php $no = 0; ?>
    @foreach($lembur AS $datalembur)
	<?php $no = $no+1; ?>
        <tr>
			<td><?php echo $no ?></td>
            <td>
				<?php
					$tgl_lembur = substr($datalembur->tgl_lembur_awal, 0, 11);
					echo $tgl_lembur;
				?>
			</td>
            <td>{{$datalembur->jenis_lembur}}</td>
            <td>{{$datalembur->nik}}</td>
            <td>{{$datalembur->nama_karyawan}}</td>
            <td>
				<?php
					$jam_lembur_awal = substr($datalembur->tgl_lembur_awal, 11, 8);
					echo $jam_lembur_awal;
				?>
			</td>
            <td>
				<?php
					$jam_lembur_akhir = substr($datalembur->tgl_lembur_akhir, 11, 8);
					echo $jam_lembur_akhir;
				?>
			</td>
            <td><?php echo $datalembur->wjm.':00' ?></td>
            <td><?php echo $datalembur->wjk.':00' ?></td>
			<td>
				<?php
					if($datalembur->batas_lembur == 'Bawah'){
						if($jam_lembur_awal > $datalembur->wjm.':00'){
							echo $datalembur->wjm.':00';
						}else{
							echo $jam_lembur_awal;
						}
					}else{
						echo $jam_lembur_awal;
					}
				?>
			</td>
            <td>
				<?php
					if($datalembur->batas_lembur == 'Bawah'){
						if($jam_lembur_akhir > $datalembur->wjk.':00'){
							echo $datalembur->wjk.':00';
						}else{
							echo $jam_lembur_akhir;
						}
					}else{
						echo $jam_lembur_akhir;
					}
				?>
			</td>
            <td>{{$datalembur->kd_divisi}}</td>
            <td>
				<?php
					if($datalembur->batas_lembur == 'Atas'){
						if($jam_lembur_akhir > $datalembur->wjk.':00'){
							$diff = abs(strtotime($jam_lembur_akhir) - strtotime($datalembur->wjk.':00'));
						}else{
							$diff = abs(strtotime($datalembur->wjk.':00') - strtotime($jam_lembur_akhir));
						}
						
						$years   = floor($diff / (365*60*60*24)); 
						$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
						$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
						
						$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60)); 
						$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
						
						echo $hours.':'.$minuts;
					}else{
						if($jam_lembur_awal > $datalembur->wjm.':00'){
							$diff = abs(strtotime($jam_lembur_awal) - strtotime($datalembur->wjm.':00'));
						}else{
							$diff = abs(strtotime($datalembur->wjm.':00') - strtotime($jam_lembur_awal));
						}
			
						$years   = floor($diff / (365*60*60*24)); 
						$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
						$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
						
						$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60)); 
						$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
						
						echo $hours.':'.$minuts;
					}
				?>
			</td>
            <td>{{$datalembur->lama_lembur}}</td>
            <td>{{$datalembur->keterangan_lembur}}</td>
            <td>{{$datalembur->app_name1}}</td>
            <td>{{$datalembur->app_name2}}</td>
            <td>{{$datalembur->app_name3}}</td>
        </tr>
    @endforeach
    </tbody>
</table>